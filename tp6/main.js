var Rx = require('rx');
var fetch = require('node-fetch');
Rx.config.longStackSupport = true;

var observer = Rx.Observer.create(
    function onNext(x) { console.log('Next: ' , x); },
    function onError(err){console.log(err)},
    function onCompleted(){console.log('Completed');}
);

function makePromise (id){
    return new Promise(function (resolve, reject){
        fetch('http://localhost:8080/pokemons/'+id)
            .then(function(response) {
                if (response.status==404){
                   reject(response);
                }
                return response.json();
            }).then(function (pokemon) {
                resolve(pokemon);
        });
    });
}


var observable = Rx.Observable
    .from(['25', '35', '45', '100', '00', '99'])
    .flatMap(a=> Rx.Observable.fromPromise(makePromise(a)))
    .catch((err) => Rx.Observable.return({label:"Pokemon non trouvé "}));


observable.subscribe(observer);






