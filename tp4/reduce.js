"use strict";
var Rx = require('rx');

var data = Rx.Observable.interval(100)
    .scan(accumulator, {sum:0, cnt:0});

function accumulator(acc, current) {
    return {sum: acc.sum + current, cnt:acc.cnt+1}
}

var observer = Rx.Observer.create(
    function onNext(x) { console.log('Next: ', x); },
    function onError(err){console.log(err)},
    function onCompleted(){ console.log('Completed');}
);

let subscription = data.subscribe (observer);
setTimeout(()=>subscription.dispose(), 3000);


