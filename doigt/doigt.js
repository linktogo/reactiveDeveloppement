"use strict";
var rx = require('rx');

var observer = rx.Rx.Observer.create(
    function onNext(x) { console.log('Next: ' + x); },
    function onError(err){console.log(err)},
    function onCompleted(){ console.log('Completed');}
);

var observable = rx.Rx.Observable.create( function (observer){
        observer.onNext('toto');
        observer.onNext('titi');
        observer.onNext('tata');
        observer.onCompleted();
}).catch( function (error){console.log(error)});


observable.subscribe(observer);

