"use strict";
var Rx = require('rx');

var data = Rx.Observable.interval(100)
    .take(10)
    .reduce(accumulator, {sum:0, cnt:0})
    .map ( (obj) => obj.sum/obj.cnt);

function accumulator(acc, current) {
    return {sum: acc.sum + current, cnt:acc.cnt+1}
}

var observer = Rx.Observer.create(
    function onNext(x) { console.log('Next: ', x); },
    function onError(err){console.log(err)},
    function onCompleted(){ console.log('Completed');}
);

data.subscribe (observer);



