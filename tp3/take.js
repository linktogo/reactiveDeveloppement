"use strict";
var Rx = require('rx');

var data = Rx.Observable.interval(500).take(15).map((a) => a*10);

var observer = Rx.Observer.create(
    function onNext(x) { console.log('Next: ' + x); },
    function onError(err){console.log(err)},
    function onCompleted(){ console.log('Completed');}
);

data.subscribe (observer);



