var Rx = require('rx');

var data = Rx.Observable.interval(1000).take(5);

var src = data.map(function(i) {
    return i * 10;
});

var o1 = Rx.Observer.create(
    function onNext(x) { console.log('o1 get ',x); },
    function onError(err) { console.log(err); },
    function onCompleted() { console.log('stream completed for o1'); }
);

var o2 = Rx.Observer.create(
    function onNext(x) { console.log('o2 get ',x); },
    function onError(err) { console.log(err); },
    function onCompleted() { console.log('stream completed for o2'); }
);

src.subscribe(o1);

// subscribe 3s later for o2
setTimeout(function() {
    src.subscribe(o2);
}, 3000);