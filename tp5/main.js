var Rx = require('rx');

var source = Rx.Observable.interval(1000).take(10);
var publisher = source.publish();

var o1 = Rx.Observer.create(
    function onNext(x) { console.log('o1 get ',x); },
    function onError(err) { console.log(err); },
    function onCompleted() { console.log('stream completed for o1'); }
);

var o2 = Rx.Observer.create(
    function onNext(x) { console.log('o2 get ',x); },
    function onError(err) { console.log(err); },
    function onCompleted() { console.log('stream completed for o2'); }
);

publisher.subscribe(o1);

publisher.connect();

setTimeout(function () {
    publisher.subscribe(o2);
}, 5000);